const express = require('express');
const bodyParser = require('body-parser');

const app = express();

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: true}));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); 
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods","POST, PUT, GET, HEAD, OPTIONS");
    next();
});

app.get("/", function(req, res) {
    res.json({ message: "Welcome to ExpressJS"});
});

require("./app/routes/book.routes.js")(app);

const port = process.env.PORT || 8081;

app.listen(port, () => {
    console.log("Server is running on port 4000");
});