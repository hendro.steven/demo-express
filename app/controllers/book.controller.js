const Book = require('../models/book.model.js');

exports.create = (req, res) =>{
    if(!req.body){
        res.status(400).send({
            message: 'Content can not be empty'
        })
    }

    const book = new Book({
        title: req.body.title,
        description: req.body.description, 
        price: req.body.price
    })

    Book.create(book,(err, data)=>{
        if(err)
            res.status(500).send({
                message: err.message || "Some error occured"
            });
        else res.send(data);
    });
}

exports.findAll = (req, res) => {
    Book.findAll((err, data)=>{
        if(err)
            res.status(500).send(
                { message: err.message || "Some error occured" }
            );
        else res.send(data);
    })
}