module.exports = app =>{

    const books = require("../controllers/book.controller.js");

    //New Book
    app.post("/books", books.create);

    app.get("/books", books.findAll);

    //app.get("/books/:bookId", books.findById);
    
}